## Name

On board data collection and interfacing for NAVROM vessel

## Description
VS9 - On board data collection and interfacing for NAVROM vessel is responsible for collecting ship-related data from vessels in UC2 (i.e., AIS data, depth, engine, speed etc.), and it is made available for other VITAL-5G NetApps and 3rd party NetApps. This NetApp collects and data specific the NAVROM vessel(s) used in UC2.


## Installation or pre-processors

Install YOLOv5

Clone repo and install requirements.txt in an environment with Python>=3.7.0  and  PyTorch>=1.7.
```
git clone https://gitlab.com/vital-5g-private/vital-5g_netapps/data-collection-navrom.git
cd yolov5
pip install -r requirements.txt
```
## Build

In order to build VS9, one should execute:

```
docker-compose build
```

## Usage

### Run YOLOv5
Running detect1.py runs inference on a variety of sources,  automatically downloading models from the latest YOLOv5 release and saving results to runs/detect

```
python3 detect1.py --weights yolov5s.pt --source  0                               # webcam
                                                  img.jpg                         # image
                                                  'rtsp://example.com/media.mp4'  # RTSP 

```

### Run Node-RED flow

To run the Node-RED flow in the background you can use:

```
docker-compose up -d
```

## Support
For support, contact [proiecte@beia.ro](mailto:proiecte@beia.ro).


## Authors and acknowledgment
This is the work of BEIA CONSULT INTERNATIONAL SRL's  staff in the context of the VITAL-5G Project.

## License
GPLv3

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
